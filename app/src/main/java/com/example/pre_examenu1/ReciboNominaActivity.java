package com.example.pre_examenu1;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import org.w3c.dom.Text;
public class ReciboNominaActivity extends AppCompatActivity{
    private ReciboNomina recibo;
    private TextView lblNombre;
    private EditText txtNumRecibo, txtNombre, txtHorasTrab, txtHorasExtras, txtSubTotal, txtImpuesto, txtTotalPagar;
    private RadioButton rdb1, rdb2, rdb3;
    private RadioGroup rdg;
    private Button btnCalcular, btnLimpiar, btnRegresar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_nomina);
        iniciarComponentes();

        MainActivity main = new MainActivity();
        lblNombre.setText(main.getTxtNombre());
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valiEmpty()){
                    Toast.makeText(getApplicationContext(),"Por favor no deje espacios vacios",Toast.LENGTH_SHORT).show();
                }
                else {
                    recibo.setNumRecibo(Integer.parseInt(txtNumRecibo.getText().toString()));
                    recibo.setNombre(txtNombre.getText().toString());
                    recibo.setHorasTrabNormal(Float.parseFloat(txtHorasTrab.getText().toString()));
                    recibo.setHorasTrabExtras(Float.parseFloat(txtHorasExtras.getText().toString()));
                    recibo.setPuesto(valirdb());

                    txtSubTotal.setText(String.valueOf(recibo.calcularSubtotal()));
                    txtImpuesto.setText(String.valueOf(recibo.calcularImpuesto()));
                    txtTotalPagar.setText(String.valueOf(recibo.calcularTotal()));
                }

            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNumRecibo.setText("");
                txtImpuesto.setText("");
                txtNombre.setText("");
                txtSubTotal.setText("");
                txtHorasTrab.setText("");
                txtHorasExtras.setText("");
                txtTotalPagar.setText("");

                rdg.clearCheck();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public int valirdb(){
        if(rdb1.isChecked()){
            return 1;
        }
        else if (rdb2.isChecked()) {
            return 2;
        }
        else if (rdb3.isChecked()) {
            return 3;
        }
        else{
            return 0;
        }
    }

    public boolean valiEmpty(){

        int rdb_empty = valirdb();
        if(txtNumRecibo.getText().toString().isEmpty() ||
        txtNombre.getText().toString().isEmpty() ||
        txtHorasExtras.getText().toString().isEmpty() ||
        txtHorasTrab.getText().toString().isEmpty() ||
        rdb_empty == 0){
            return true;
        }
        else {
            return false;
        }
    }
    public void iniciarComponentes(){
        ReciboNomina recibo = new ReciboNomina();

        lblNombre = findViewById(R.id.lblNombre);

        txtNumRecibo = findViewById(R.id.txtNumRecibo);
        txtNombre = findViewById(R.id.txtNombre);
        txtHorasTrab = findViewById(R.id.txtHorasTrab);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        txtSubTotal = findViewById(R.id.txtSubTotal);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        txtTotalPagar = findViewById(R.id.txtTotal);

        rdb1 = findViewById(R.id.rdb1);
        rdb2 = findViewById(R.id.rdb2);
        rdb3 = findViewById(R.id.rdb3);

        rdg = findViewById(R.id.radioGroup);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
    }
    
}
