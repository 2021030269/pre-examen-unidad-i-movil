package com.example.pre_examenu1;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float inpuestoPorc;

    public ReciboNomina() {
        this.numRecibo = 0;
        this.nombre = "";
        this.horasTrabNormal = 0.0f;
        this.horasTrabExtras = 0.0f;
        this.puesto = 0;
    }

    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
    }

    private float impuestoPorc(){
        return calcularImpuesto() * 100 / calcularSubtotal();
    }



    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float calcularSubtotal(){
        float pago_base = 0;

        if (this.puesto == 1){
            pago_base = (float) (200 * 1.2);
        } else if (this.puesto == 2) {
            pago_base = (float) (200*1.5);
        }
        else{
            pago_base = (float)(200*2);
        }
        return (pago_base * this.horasTrabNormal) + (pago_base*(this.horasTrabExtras*2));
    }

    public float calcularImpuesto(){
        return (float) (calcularSubtotal()*0.16);
    }

    public float calcularTotal(){
        return this.calcularSubtotal() - this.calcularImpuesto();
    }
}
